import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JPanel;

public class DrawPanel extends JPanel implements Runnable {

	private double sx, sy;
	private double time;
	private final double G = 9.80665;
	public static Queue<Play> queue = new LinkedList<Play>();
	private List<Point> pts;
	private Play actual;

	public DrawPanel() {
		this.setBounds(0, 0, 801, 601);
		sx = 50;
		sy = 540;
		setVisible(true);
		// queue =
		pts = new LinkedList<Point>();
	}

	private double getObjX(double t, double angle) {
		return sx + Math.cos(angle * Math.PI / 180.) * actual.velocity * t;
	}

	private double getObjY(double t, double angle) {
		return sy + actual.velocity * Math.sin(angle * Math.PI / 180.) * t + (-G * (t * t)) / 2.;
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (actual == null)
			return;
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.drawRect(16, 16, 782, 550);
		g2d.setColor(Color.WHITE);
		g2d.fillRect(17, 17, 781, 549);

		g2d.setColor(Color.BLACK);
		double X = getObjX(time, actual.angle);
		double Y = getObjY(time, actual.angle);

		g2d.drawLine((int) sx - 15, (int) sy, (int) sx + 720, (int) sy);
		g2d.drawLine((int) sx, (int) sy + 15, (int) sx, (int) sy - 470);

		g2d.setColor(Color.GRAY);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (int i = 0; i < pts.size() - 1; i++) {
			if (i % 2 == 1)
				g2d.drawLine(pts.get(i).x, pts.get(i).y, pts.get(i + 1).x, pts.get(i + 1).y);
		}
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		g2d.setColor(Color.BLACK);
		System.out.println();
		if (Y < sy) {
			double tmpX = pts.get(pts.size() - 1).x;
			double tmpY = pts.get(pts.size() - 1).y;
			g2d.drawOval((int) tmpX - 6, (int) tmpY - 6, 12, 12);
			actual.isRunning = false;
			time = 0;
			pts.clear();
			return;
		} else {
			time += 0.07;
		}
		if (actual.isRunning) {
			if (Y >= sy) {
				pts.add(new Point((int) X, (int) sy - Math.abs((int) sy - (int) Y)));
				g2d.drawOval((int) X - 6, (int) sy - Math.abs((int) sy - (int) Y) - 6, 12, 12);
			} else {
				pts.add(new Point((int) X, (int) sy + Math.abs((int) sy - (int) Y)));
				g2d.drawOval((int) X - 6, (int) sy + Math.abs((int) sy - (int) Y - 6), 12, 12);
			}
		}
		g2d.dispose();
	}

	@Override
	public void run() {
		while (true) {
			if (actual != null && !actual.isRunning && queue.isEmpty()) {
				continue;
			} else if (actual != null && actual.isRunning) {
				repaint();
			} else if ((actual != null && !actual.isRunning && !queue.isEmpty()) || (actual == null && !queue.isEmpty())) {
				actual = queue.poll();
			}
			try {
				Thread.sleep(7);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
}
