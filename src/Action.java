import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Action extends JPanel {
	private JTextField jtf_angle;
	private JTextField jtf_vel;
	private JButton btnNewButton;
	
	public Action() {
		this.setBounds(0, 0, 180, 100);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
		
		JLabel lb_angle = new JLabel("Angle \u00B0:");
		lb_angle.setBounds(10, 10, 60, 14);
		add(lb_angle);
		
		jtf_angle = new JTextField();
		jtf_angle.setBounds(80, 7, 86, 20);
		jtf_angle.setColumns(10);
		add(jtf_angle);
		
		JLabel lb_vel = new JLabel("Velocity:");
		lb_vel.setBounds(10, 38, 59, 14);
		add(lb_vel);
		
		jtf_vel = new JTextField();
		jtf_vel.setColumns(10);
		jtf_vel.setBounds(80, 35, 86, 20);
		add(jtf_vel);
		
		btnNewButton = new JButton("Send to queue");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Play p = new Play();
				p.angle = Double.parseDouble(jtf_angle.getText());
				p.velocity = Double.parseDouble(jtf_vel.getText());
				DrawPanel.queue.add(p);
			}
		});
		btnNewButton.setBounds(10, 66, 155, 23);
		add(btnNewButton);

	}
}
