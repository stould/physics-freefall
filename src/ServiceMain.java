import java.awt.Color;

import javax.swing.JFrame;

public class ServiceMain extends JFrame {
	
	public ServiceMain() {
		setBounds(0, 0, 1007, 602);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//getContentPane().setLayout(null);
		Action ac = new Action();
		ac.setLocation(811, 11);
		ac.setVisible(true);
		getContentPane().add(ac);
		getContentPane().setLayout(null);
		DrawPanel dp = new DrawPanel();
		dp.setBounds(0, 0, 801, 601);
		Thread thr = new Thread(dp);
		thr.start();
		getContentPane().add(dp);
	}
	
}
